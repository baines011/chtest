@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Interview Types</div>

                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    @foreach($interviewTypes as $interviewType)
                        <tr>
                            <td>{{ $interviewType->id }}</td>
                            <td>{{ $interviewType->name }}</td>
                            <td>@if($interviewType->status) Active @else Inactive @endif</td>
                            <td>{{ Html::link(route('interviewtype.edit', ['id' => $interviewType->id]), 'Edit')}}</td>
                        </tr>
                    @endforeach
                    </table>
                    {{ Html::link(route('interviewtype.create'), 'Add Interview Type')}}
                    {{ $interviewTypes->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
