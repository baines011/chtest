@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create interview type</div>

                    <div class="panel-body">
                        {{ Form::open(array('route' => array('interviewtype.store'), 'method' => 'post')) }}
                        <div>
                            {{ Form::label('name', 'Name') }}</div>
                        <div>
                            {{ Form::text('name') }}
                        </div>
                        <div>
                            {{ Form::label('description', 'Description') }}</div>
                        <div>
                            {{ Form::textarea('description') }}
                        </div>
                        <div>
                            {{ Form::label('is_active', 'Status') }}</div>
                        <div>
                            {{ Form::select('is_active', [1 => 'Active', 0 => 'Inactive']) }}
                        </div>
                        <div>
                            {{ csrf_field() }}
                            {{  Form::submit('Save') }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
