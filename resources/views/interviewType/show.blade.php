@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Interview Type</div>

                    <div class="panel-body">
                        <div>
                            User:
                            {{ $interviewType->name }}
                        </div>
                        <div>
                            Description: <br>
                            {{ $interviewType->description }}
                        </div>
                        <div>
                            Status:
                            @if($interviewType->is_active) Active @else Inactive @endif
                        </div>
                        <div>
                            {{ Html::link(route('interviewtype.edit', ['id' => $interviewType->id]), 'Edit', ['class' => 'btn btn-default'])}}

                            {{ Form::model($interviewType, array('route' => array('interviewtype.destroy', $interviewType->id))) }}
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                {{  Form::submit('Delete') }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
