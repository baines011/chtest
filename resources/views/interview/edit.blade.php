@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit interview</div>

                    <div class="panel-body">
                        {{ Form::model($interview, array('route' => array('interview.update', $interview->id))) }}
                        <div>
                            {{ Form::label('user_id', 'User') }}</div>
                        <div>
                            {{ Form::text('user_id') }}
                        </div>
                        <div>
                            {{ Form::label('interview_type_id', 'Interview Type') }}</div>
                        <div>
                            {{ Form::select('interview_type_id', $interviewTypes) }}
                        </div>
                        <div>
                            {{ Form::label('start_at', 'Start At') }}</div>
                        <div>
                            {{ Form::date('start_at') }}
                        </div>
                        <div>
                            {{ Form::label('status', 'Status') }}</div>
                        <div>
                            {{ Form::select('status', [1 => 'Active', 0 => 'Inactive']) }}
                        </div>
                        <div>
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            {{  Form::submit('Save') }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
