@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Interviews</div>

                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>ID</th>
                            <th>User</th>
                            <th>Type</th>
                            <th>Start at</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    @foreach($interviews as $interview)
                        <tr>
                            <td>{{ $interview->id }}</td>
                            <td>{{ $interview->user->last_name }} {{ $interview->user->first_name }}</td>
                            <td>{{ $interview->interviewType->name }}</td>
                            <td>{{ $interview->start_at }}</td>
                            <td>@if($interview->status) Active @else Inactive @endif</td>
                            <td>{{ Html::link(route('interview.edit', ['id' => $interview->id]), 'Edit')}}</td>
                        </tr>
                    @endforeach
                    </table>
                    {{ Html::link(route('interview.create'), 'Sign up for an interview')}}
                    {{ $interviews->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
