@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Intervied Data</div>

                    <div class="panel-body">
                        <div>
                            User:
                            {{ $interview->user->first_name }}
                            {{ $interview->user->last_name }}
                        </div>
                        <div>
                            Phone:
                            {{ $interview->interviewType->name }}
                        </div>
                        <div>
                            Status:
                            @if($interview->status) Active @else Inactive @endif
                        </div>
                        <div>
                            Start at: {{ $interview->start_at }}
                        </div>
                        <div>
                            {{ Html::link(route('interview.edit', ['id' => $interview->id]), 'Edit', ['class' => 'btn btn-default'])}}

                            {{ Form::model($interview, array('route' => array('interview.destroy', $interview->id))) }}
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                {{  Form::submit('Delete') }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
