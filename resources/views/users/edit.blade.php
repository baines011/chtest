@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit user:{{ $user->last_name }} {{ $user->first_name }}</div>

                    <div class="panel-body">
                        {{ Form::model($user, array('route' => array('users.update', $user->id))) }}
                        <div>
                            {{ Form::label('first_name', 'First name') }}</div>
                        <div>
                            {{ Form::text('first_name') }}
                        </div>
                        <div>
                            {{ Form::label('last_name', 'Last name') }}</div>
                        <div>
                            {{ Form::text('last_name') }}
                        </div>
                        <div>
                            {{ Form::label('email', 'email') }}</div>
                        <div>
                            {{ Form::text('email', $user->email, array('disabled' => 'disabled')) }}
                        </div>
                        <div>
                            {{ Form::label('phone', 'phone') }}</div>
                        <div>
                            {{ Form::text('phone') }}
                        </div>
                        <div>
                            {{ Form::label('is_active', 'Active') }}</div>
                        <div>
                            {{ Form::checkbox('is_active') }}
                        </div>
                        <div>
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            {{  Form::submit('Save') }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
