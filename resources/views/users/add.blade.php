@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create user</div>

                    <div class="panel-body">
                        {{ Form::open(array('route' => array('users.store'), 'method' => 'post')) }}
                        <div>
                            {{ Form::label('first_name', 'First name') }}</div>
                        <div>
                            {{ Form::text('first_name') }}
                        </div>
                        <div>
                            {{ Form::label('last_name', 'Last name') }}</div>
                        <div>
                            {{ Form::text('last_name') }}
                        </div>
                        <div>
                            {{ Form::label('email', 'email') }}</div>
                        <div>
                            {{ Form::text('email') }}
                        </div>
                        <div>
                            {{ Form::label('Phone', 'phone') }}</div>
                        <div>
                            {{ Form::text('phone') }}
                        </div>
                        <div>
                            {{ Form::label('password', 'Password') }}</div>
                        <div>
                            {{ Form::password('password') }}
                        </div>
                        <div>
                            {{ Form::label('role_id', 'Role') }}</div>
                        <div>
                            {{ Form::select('role_id', $roles) }}
                        </div>
                        <div>
                            {{ Form::label('is_active', 'Active') }}</div>
                        <div>
                            {{ Form::checkbox('is_active') }}
                        </div>
                        <div>
                            {{ csrf_field() }}
                            {{  Form::submit('Save') }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
