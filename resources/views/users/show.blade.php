@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $user->last_name }} {{ $user->first_name }}</div>

                    <div class="panel-body">
                        <div>
                            Email:
                            {{ $user->email }}
                        </div>
                        <div>
                            Phone:
                            {{ $user->phone }}
                        </div>
                        <div>
                            Is Active:
                            @if($user->is_active) Yes @else No @endif
                        </div>
                        <div>
                            Role: {{ $user->role->name }}
                        </div>
                        <div>
                            {{ Html::link(route('users.edit', ['id' => $user->id]), 'Edit', ['class' => 'btn btn-default'])}}

                            {{ Form::model($user, array('route' => array('users.destroy', $user->id))) }}
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                            {{  Form::submit('Delete') }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
