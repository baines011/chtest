<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // admin
        DB::table('users')->insert([
            'role_id'   => App\Models\Role::where('name', 'admin')->first()->id,
            'first_name'      => 'Pera',
            'last_name'      => 'Perić',
            'email'     => 'admissions@cloudhorizon.com',
            'password'  => bcrypt('admissions'),
            'is_active' => 1,
            'created_at'=> date("Y-m-d H:i:s"),
            'updated_at'=> date("Y-m-d H:i:s"),

        ]);

        // create test users
        //$user = factory(App\Models\User::class, 50)->create();
    }
}
