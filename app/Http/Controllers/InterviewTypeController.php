<?php

namespace App\Http\Controllers;

use App\Models\Interview;
use App\Models\InterviewType;
use Illuminate\Http\Request;

class InterviewTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviewTypes = InterviewType::paginate(15);

        return view('interviewType.index', ['interviewTypes' => $interviewTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('interviewType.add', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'is_active' => 'required|boolean',
        ]);

        $interviewType = InterviewType::create([
            'name' => $data['name'],
            'description' => $data['description'],
            'is_active' => $data['is_active'],
        ]);

        return view('interviewType.show', ['interviewType' => $interviewType]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InterviewType  $interviewType
     * @return \Illuminate\Http\Response
     */
    public function show(InterviewType $interviewType)
    {
        return view('interviewType.show', ['interviewType' => $interviewType]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InterviewType  $interviewType
     * @return \Illuminate\Http\Response
     */
    public function edit(InterviewType $interviewType)
    {
        return view('interviewType.edit', ['interviewType' => $interviewType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InterviewType  $interviewType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InterviewType $interviewType)
    {
        $data = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'is_active' => 'required|boolean',
        ]);


        $interviewType->update([
            'name' => $data['name'],
            'description' => $data['description'],
            'is_active' => $data['is_active'],
        ]);

        return redirect()->route('interviewtype.show', $interviewType->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InterviewType  $interviewType
     * @return \Illuminate\Http\Response
     */
    public function destroy(InterviewType $interviewType)
    {
        $interviewType->delete();

        return redirect()->route('interviewtype.index');
    }
}
