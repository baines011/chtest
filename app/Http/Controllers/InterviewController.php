<?php

namespace App\Http\Controllers;

use App\Models\Interview;
use App\Models\InterviewType;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::paginate(15);

        return view('interview.index', ['interviews' => $interviews]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('role_id', Role::where('name', 'student')->first()->id)->pluck('last_name', 'id');
        $interviewTypes = InterviewType::all()->pluck('name', 'id');

        return view('interview.add', ['interviewTypes' => $interviewTypes, 'users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'user_id' => 'exists:users,id',
            'interview_type_id' => 'exists:interview_types,id',
            'start_at' => 'required|date|after_or_equal:now',
            'status' => 'required|boolean',
        ]);

        $interview = Interview::create([
            'user_id' => $data['user_id'],
            'interview_type_id' => $data['interview_type_id'],
            'start_at' => $data['start_at'],
            'status'   => $data['status'],
        ]);

        return view('interview.show', ['interview' => $interview]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show(Interview $interview)
    {
        return view('interview.show', ['interview' => $interview]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit(Interview $interview)
    {
        return view('interview.edit', ['interview' => $interview]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Interview $interview)
    {
        $data = $request->validate([
            'user_id' => 'exists:users,id',
            'interviewuser_id_type_id' => 'exists:interview_types,id',
            'start_at' => 'required|date|after_or_equal:now',
            'status' => 'required|boolean',
        ]);

        $interview->update([
            'user_id' => $data['user_id'],
            'interview_type_id' => $data['interview_type_id'],
            'start_at' => $data['start_at'],
            'status' => $data['status'],
        ]);

        return redirect()->route('interview.show', $interview->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interview $interview)
    {
        $interview->delete();

        return redirect()->route('interview.index');
    }
}
