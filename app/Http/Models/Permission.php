<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $created_at
 * @property string $label
 * @property string $name
 * @property string $updated_at
 * @property Role[] $roles
 */
class Permission extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['created_at', 'label', 'name', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'permissions_roles');
    }
}
