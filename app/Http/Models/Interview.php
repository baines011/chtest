<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\InterviewType;
use App\Models\User;

/**
 * @property int $id
 * @property int $user_id
 * @property int $interview_type_id
 * @property string $start_at
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property InterviewType $interviewType
 */
class Interview extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        '   id',
        'user_id',
        'interview_type_id',
        'start_at',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function interviewType()
    {
        return $this->belongsTo(InterviewType::class);
    }
}
