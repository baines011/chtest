<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Interview;
use App\Models\Role;

/**
 * Class User
 *
 * @property int $id
 * @property int $role_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property boolean $is_active
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property Interview[] $interviews
 *
 * @property \App\Models\Role $role
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $perPage = 50; //set number of items per page

    /**
     * The attributes that sets variable type.
     *
     * @var array
     */
    protected $casts = [
        'role_id' => 'int',
        'is_active' => 'int'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'is_active',
    ];

    /**
     * The role that user belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Interviews made by user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interviews()
    {
        return $this->hasMany(Interview::class, 'user_id');
    }

}
